2005-11-25  Matthias Clasen  <mclasen@redhat.com>

	* patches/patch-order.xml:
	* patches/matthias-ellipsis-shaping.diff: Add a proof-of-concept
	patch which avoids redundant shaping of the ellipsis during
	ellipsisation.

2005-11-22  Behdad Esfahbod  <behdad@gnome.org>

	* patches/behdad-glyph-extent-hash.patch: Added.

2005-11-22  Behdad Esfahbod  <behdad@gnome.org>

	* Makefile: Added.

2005-11-22  Federico Mena Quintero  <federico@ximian.com>

	* pango-language-profile.c (measure_strings): Turn on a width of
	200 pixels and PANGO_ELLIPSIZE_END for the layouts.  This makes
	_pango_layout_line_ellipsize() pop up in the profile at 25% of the
	time.

2005-11-10  Behdad Esfahbod  <behdad@gnome.org>

	* patches/patch-order.xml: Removed
	fmq-easy-script-and-paired-chars-tables.diff; it's on Pango CVS now.

2005-11-04  Federico Mena Quintero  <federico@ximian.com>

	* patch-and-plot.py: New file; untested and unfinished code to
	patch pango automatically and get timings for each patch.

	* plot-languages.py (get_chart_colors): Make the charts a bit more fun.
	(LanguagePlot.__init__): Remove some stale code.
	(default_palette): Add some porn-based colors, courtesy of Hans Petter.

2005-11-04  Federico Mena Quintero  <federico@ximian.com>

	* patches/patch-order.xml: Removed
	fmq-gunichar-to-glyph-index.diff; it's on Pango CVS now.

2005-11-04  Federico Mena Quintero  <federico@ximian.com>

	* patches/fmq-gunichar-to-glyph-index.diff: Make the cache
	counters longs instead of ints.  Use #defines for the cache
	parameters (GLYPH_CACHE_NUM_ENTRIES, GLYPH_CACHE_MASK) instead of
	explicit numeric values.

2005-11-03  Federico Mena Quintero  <federico@ximian.com>

	* patches/fmq-easy-script-and-paired-chars-tables.diff: Link to
	bug #320666.

	* patches/fmq-gunichar-to-glyph-index.diff: Link to bug #320665.

2005-11-03  Billy Biggs <vektor@dumbterm.net>

	* patches/vektor-breakfast.diff: Tasty.

2005-11-03  Billy Biggs <vektor@dumbterm.net>

	* patches/vektor-breakfast.diff: Surely broken
	proof-of-concept.

2005-11-03  Behdad Esfahbod <behdad@gnome.org>

	* patches/behdad-mini-fribidi-utf8.patch: Applied.

2005-11-03  Behdad Esfahbod <behdad@gnome.org>

	* patches/behdad-opentype-calltable-misc.patch: Applied.

2005-11-03  Behdad Esfahbod <behdad@gnome.org>

	* patches/behdad-mini-fribidi-utf8.patch:
	* patches/behdad-opentype-calltable-misc.patch:
	Added.

2005-11-03  Billy Biggs <vektor@dumbterm.net>

	* patches/vektor-save-a-memcpy.diff: Silly patch that improves
	timings for me.

	* patches/patch-order.xml: Add it to the list.

2005-11-02  Billy Biggs <vektor@dumbterm.net>

	* patches/vektor-glyph-extent-hash.diff: Add a patch to increase
	the size of the hash table used to cache glyph extents.

	* patches/patch-order.xml: Add my patch to the list.

2005-10-31  Federico Mena Quintero  <federico@ximian.com>

	* plot-languages.py (main): Patch from Kalle Vahlman to call
	setlocale(LC_ALL, "") at the beginning of the program.

2005-10-27  Federico Mena Quintero  <federico@ximian.com>

	* README: Add a README with usage information and other details.

2005-10-26  Federico Mena Quintero  <federico@ximian.com>

	* ChangeLog: Start the ChangeLog.

	* pango-language-profile.c: Current state of the multi-language
	benchmark program for Pango.

	* plot-languages.py: Current state of the plotting program for the
	results computed with pango-language-profile.

	* po-data/*: Language data files.

	* patches/*: Set of optimization patches for Pango.
