#!/usr/bin/env python
#
# patch-pango-and-plot.py - Apply a set of patches on Pango, and take timings
#
# Copyright (C) 2005 Federico Mena-Quintero
# federico@novell.com

import sys
import locale
import optparse
import math
import cairo
from xml.dom import minidom, Node

pango_dir = None

def string_from_node (node):
    c = node.childNodes[0]
    if c.nodeType == Node.TEXT_NODE:
        return c.nodeValue
    else:
        return None;

def read_patches ():
    # FIXME: read patches/patch-order.xml and return a list of (patch_filename, human_readable_name)
    # That file looks like
    #  <patch>
    #    <file>some-patch.diff</file>
    #    <name>patch that frobs the glyph foo</name>
    #  </patch>
    #  ...

def apply_patch (patch_file):
    if patch_file:                      # None indicates an unpatched pango
        if os.system ("patch -d %s -p0 < %s" % (pango_dir, patch_file)) != 0:
            print "Failed while applying patch %s" % patch_file
            sys.exit (1)

def build_pango (patch_file):
    if os.system ("cd %s && make && make install" % pango_dir) != 0:
        if patch_file:
            print 'Failed to "make" and "make install" pango with patch %s' % patch_file
        else:
            print 'Failed to "make" and "make install" pango with no patches'

        sys.exit (1)

def run_benchmark (patch_file, patch_name):
    if patch_file:
        result_file = os.path.basename (patch_file) + ".xml"
    else:
        result_file = "pango-original.xml"

    if os.system ("./pango-language-profile -o %s -n '%s'" % (result_file, patch_name)) != 0:
        print "Failed to run pango-language-profile for result file %s" % result_file
        sys.exit (1)

def run_patch (patch):
    (patch_file, patch_name) = patch

    apply_patch (patch_file)
    build_pango (patch_file)
    run_benchmark (patch_file, patch_name)

def main ():
    option_parser = optparse.OptionParser (usage="usage: %prog -o output.png")
    option_parser.add_option ("-o", "--output", dest="output", metavar="FILE",
                              help="Name of output file (output is a PNG file)")
    option_parser.add_option (None, "--pango-dir", dest="pango_dir", metavar="DIR",
                              help="Source directory for Pango")

    (options, args) = option_parser.parse_args ()

    if not options.output:
        print 'Please specify an output filename with "-o file.png" or "--output=file.png".'
        sys.exit (1)

    if not options.pango_dir:
        print 'Please specify the pango source directory with "--pango-dir=~/src/pango"'
        sys.exit (1)

    out_filename = options.output
    pango_dir = options.pango_dir

    patches = read_patches ()
    for p in patches:
        run_patch (p)

    # FIXME: do the plot

main ()
