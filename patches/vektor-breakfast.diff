Index: pango/break.c
===================================================================
RCS file: /cvs/gnome/pango/pango/break.c,v
retrieving revision 1.31.2.1
diff -p -u -r1.31.2.1 break.c
--- pango/break.c	26 Aug 2005 18:53:33 -0000	1.31.2.1
+++ pango/break.c	4 Nov 2005 00:25:07 -0000
@@ -410,6 +410,394 @@ typedef enum
 
 
 /**
+ * pango_default_breakfast:
+ * @text: text to break
+ * @length: length of text in bytes (may be -1 if @text is nul-terminated)
+ * @analysis: a #PangoAnalysis for the @text
+ * @attrs: logical attributes to fill in
+ * @attrs_len: size of the array passed as @attrs
+ *
+ * This is the default break algorithm, used if no language
+ * engine overrides it. Normally you should use pango_break()
+ * instead; this function is mostly useful for chaining up
+ * from a language engine override. Unlike pango_break(),
+ * @analysis can be NULL, but only do that if you know what
+ * you're doing. (If you need an analysis to pass to pango_break(),
+ * you need to pango_itemize() or use pango_get_log_attrs().)
+ *
+ **/
+void
+pango_default_breakfast (const gchar   *text,
+                         gint           length,
+                         PangoAnalysis *analysis,
+                         PangoLogAttr  *attrs,
+                         int            attrs_len)
+{
+  const gchar *next;
+  gint i;
+  gunichar prev_wc;
+  gunichar next_wc;
+  GUnicodeBreakType next_break_type;
+  GUnicodeType prev_type;
+  GUnicodeBreakType prev_break_type; /* skips spaces */
+  gboolean prev_was_break_space;
+  gunichar base_character = 0;
+  gint n_chars;
+
+  g_return_if_fail (text != NULL);
+  g_return_if_fail (attrs != NULL);
+
+  n_chars = g_utf8_strlen (text, length);
+
+  next = text;
+  
+  /* + 1 because of the extra newline we stick on the end */
+  if (attrs_len < n_chars + 1)
+    {
+      g_warning ("pango_default_break(): the array of PangoLogAttr passed in must have at least N+1 elements, if there are N characters in the text being broken");
+      return;
+    }
+      
+  prev_type = (GUnicodeType) -1;
+  prev_break_type = G_UNICODE_BREAK_UNKNOWN;
+  prev_was_break_space = FALSE;
+  prev_wc = 0;
+
+  if (n_chars)
+    {
+      next_wc = g_utf8_get_char (next);
+      g_assert (next_wc != 0);
+    }
+  else
+    next_wc = '\n';
+
+  next_break_type = g_unichar_break_type (next_wc);
+  next_break_type = BREAK_TYPE_SAFE (next_break_type);
+
+  for (i = 0; i <= n_chars; i++)
+    {
+      GUnicodeType type;
+      gunichar wc;
+      GUnicodeBreakType break_type;
+      BreakOpportunity break_op;
+
+      wc = next_wc;
+      break_type = next_break_type;
+
+      if (i == n_chars)
+        {
+          /*
+           * If we have already reached the end of @text g_utf8_next_char()
+           * may not increment next
+           */
+          next_wc = 0;
+	  next_break_type = G_UNICODE_BREAK_UNKNOWN;
+        }
+      else
+        {
+          next = g_utf8_next_char (next);
+
+          if (i == n_chars - 1)
+            {
+              /* This is how we fill in the last element (end position) of the
+               * attr array - assume there's a newline off the end of @text.
+               */
+              next_wc = '\n';
+            }
+          else
+            {
+              next_wc = g_utf8_get_char (next);
+              g_assert (next_wc != 0);
+            }
+	  
+	  next_break_type = g_unichar_break_type (next_wc);
+          next_break_type = BREAK_TYPE_SAFE (next_break_type);
+        }
+
+      type = g_unichar_type (wc);
+
+      /* ---- Cursor position breaks (Grapheme breaks) ---- */
+
+      if (wc == '\n')
+        {
+          /* Break before line feed unless prev char is a CR */
+
+          if (prev_wc != '\r')
+            attrs[i].is_cursor_position = TRUE;
+          else
+            attrs[i].is_cursor_position = FALSE;
+        }
+      else if (i == 0 ||
+               prev_type == G_UNICODE_CONTROL ||
+               prev_type == G_UNICODE_FORMAT)
+        {
+          /* Break at first position (must be special cased, or if the
+           * first char is say a combining mark there won't be a
+           * cursor position at the start, which seems wrong to me
+           * ???? - maybe it makes sense though, who knows)
+           */
+          /* break after all format or control characters */
+          attrs[i].is_cursor_position = TRUE;
+        }
+      else
+        {
+          switch (type)
+            {
+            case G_UNICODE_CONTROL:
+            case G_UNICODE_FORMAT:
+              /* Break before all format or control characters */
+              attrs[i].is_cursor_position = TRUE;
+              break;
+
+            case G_UNICODE_COMBINING_MARK:
+            case G_UNICODE_ENCLOSING_MARK:
+            case G_UNICODE_NON_SPACING_MARK:
+              /* Unicode spec includes "Combining marks plus Tibetan
+               * subjoined characters" as joining chars, but lists the
+               * Tibetan subjoined characters as combining marks, and
+               * g_unichar_type() returns NON_SPACING_MARK for the Tibetan
+               * subjoined characters. So who knows, beats me.
+               */
+
+              /* It's a joining character, break only if preceded by
+               * control or format; we already handled the case where
+               * it was preceded earlier, so here we know it wasn't,
+               * don't break
+               */
+              attrs[i].is_cursor_position = FALSE;
+              break;
+
+            case G_UNICODE_LOWERCASE_LETTER:
+            case G_UNICODE_MODIFIER_LETTER:
+            case G_UNICODE_OTHER_LETTER:
+            case G_UNICODE_TITLECASE_LETTER:
+            case G_UNICODE_UPPERCASE_LETTER:
+              if (JAMO (wc))
+                {
+                  /* Break before Jamo if they are in a broken sequence or
+                   * next to non-Jamo, otherwise don't
+                   */
+                  if (LEADING_JAMO (wc) &&
+                      !LEADING_JAMO (prev_wc))
+                    attrs[i].is_cursor_position = TRUE;
+                  else if (VOWEL_JAMO (wc) &&
+                           !LEADING_JAMO (prev_wc) &&
+                           !VOWEL_JAMO (prev_wc))
+                    attrs[i].is_cursor_position = TRUE;
+                  else if (TRAILING_JAMO (wc) &&
+                           !LEADING_JAMO (prev_wc) &&
+                           !VOWEL_JAMO (prev_wc) &&
+                           !TRAILING_JAMO (prev_wc))
+                    attrs[i].is_cursor_position = TRUE;
+                  else
+                    attrs[i].is_cursor_position = FALSE;
+                }
+              else
+                {
+                  /* Handle non-Jamo non-combining chars */
+
+                  /* Break if preceded by Jamo; don't break if a
+                   * letter is preceded by a virama; break in all
+                   * other cases. No need to check whether we're
+                   * preceded by Jamo explicitly, since a Jamo is not
+                   * a virama, we just break in all cases where we
+                   * aren't preceded by a virama. Don't fool with viramas
+                   * if we aren't part of a script that uses them.
+                   */
+
+                  if (VIRAMA_SCRIPT (wc))
+                    {
+                      /* Check whether we're preceded by a virama; this
+                       * could use some optimization.
+                       */
+                      if (VIRAMA (prev_wc))
+                        attrs[i].is_cursor_position = FALSE;
+                      else
+                        attrs[i].is_cursor_position = TRUE;
+                    }
+                  else
+                    {
+                      attrs[i].is_cursor_position = TRUE;
+                    }
+                }
+              break;
+
+            default:
+              /* Some weirdo char, just break here, why not */
+              attrs[i].is_cursor_position = TRUE;
+              break;
+            }
+        }
+      
+      /* ---- Line breaking ---- */
+
+      break_op = BREAK_ALREADY_HANDLED;
+
+      g_assert (prev_break_type != G_UNICODE_BREAK_SPACE);
+
+      attrs[i].is_line_break = FALSE;
+      
+      if (attrs[i].is_cursor_position) /* If it's not a grapheme boundary,
+                                        * it's not a line break either
+                                        */
+        {
+	  /* space followed by a combining mark is handled
+	   * specially; (rule 7a from TR 14)
+	   */
+	  if (break_type == G_UNICODE_BREAK_SPACE &&
+	      next_break_type == G_UNICODE_BREAK_COMBINING_MARK)
+	    break_type = G_UNICODE_BREAK_IDEOGRAPHIC;
+		
+          /* Unicode doesn't specify char wrap; we wrap around all chars
+           * except where a line break is prohibited, which means we
+           * effectively break everywhere except inside runs of spaces.
+           */
+          attrs[i].is_char_break = TRUE;          
+          
+          switch (prev_break_type)
+            {
+            case G_UNICODE_BREAK_MANDATORY:
+            case G_UNICODE_BREAK_LINE_FEED:
+            case G_UNICODE_BREAK_NEXT_LINE:
+              attrs[i].is_line_break = TRUE;
+              break;
+
+            case G_UNICODE_BREAK_CARRIAGE_RETURN:
+              if (wc != '\n')
+                {
+                  attrs[i].is_line_break = TRUE;
+                }
+              break;
+
+            case G_UNICODE_BREAK_CONTINGENT:
+              /* can break after 0xFFFC by default, though we might want
+               * to eventually have a PangoLayout setting or
+               * PangoAttribute that disables this, if for some
+               * application breaking after objects is not desired.
+               */
+              break_op = BREAK_ALLOWED;
+              break;
+
+            case G_UNICODE_BREAK_SURROGATE:
+	      g_assert_not_reached ();
+              break;
+
+            case G_UNICODE_BREAK_AMBIGUOUS:
+              /* FIXME we need to resolve the East Asian width
+               * to decide what to do here
+               */
+            case G_UNICODE_BREAK_COMPLEX_CONTEXT:
+              /* FIXME language engines should handle this case... */
+            case G_UNICODE_BREAK_UNKNOWN:
+              /* treat unknown, complex, ambiguous as if they were
+               * alphabetic for now.
+               */
+              prev_break_type = G_UNICODE_BREAK_ALPHABETIC;
+              /* FALL THRU to use the pair table if appropriate */
+
+            default:
+
+              /* Note that our table assumes that combining marks
+               * are only applied to alphabetic characters;
+               * tech report 14 explains how to remove this assumption
+               * from the code, if anyone ever cares, but it shouldn't
+               * be a problem. Also this issue sort of goes
+               * away since we only look for breaks on grapheme
+               * boundaries.
+               */
+
+              g_assert (IN_BREAK_TABLE (prev_break_type));
+
+              switch (break_type)
+                {
+                case G_UNICODE_BREAK_MANDATORY:
+                case G_UNICODE_BREAK_LINE_FEED:
+                case G_UNICODE_BREAK_CARRIAGE_RETURN:
+                case G_UNICODE_BREAK_NEXT_LINE:
+                case G_UNICODE_BREAK_SPACE:
+                  /* These types all "pile up" at the end of lines and
+                   * get elided.
+                   */
+                  break_op = BREAK_PROHIBITED;
+                  break;
+
+                case G_UNICODE_BREAK_CONTINGENT:
+                  /* break before 0xFFFC by default, eventually
+                   * make this configurable?
+                   */
+                  break_op = BREAK_ALLOWED;
+                  break;
+
+                case G_UNICODE_BREAK_AMBIGUOUS:
+                  /* FIXME resolve East Asian width to figure out what to do */
+                case G_UNICODE_BREAK_COMPLEX_CONTEXT:
+                  /* FIXME language engine analysis */
+                case G_UNICODE_BREAK_UNKNOWN:
+                case G_UNICODE_BREAK_ALPHABETIC:
+                  /* treat all of the above as alphabetic for now */
+                  break_op = BREAK_OP (prev_break_type, G_UNICODE_BREAK_ALPHABETIC);
+                  break;
+
+                case G_UNICODE_BREAK_SURROGATE:
+		  g_assert_not_reached ();
+                  break;
+
+                default:
+                  g_assert (IN_BREAK_TABLE (prev_break_type));
+                  g_assert (IN_BREAK_TABLE (break_type));
+                  break_op = BREAK_OP (prev_break_type, break_type);
+                  break;
+                }
+              break;
+            }
+
+          if (break_op != BREAK_ALREADY_HANDLED)
+            {
+              switch (break_op)
+                {
+                case BREAK_PROHIBITED:
+                  /* can't break here */                  
+                  attrs[i].is_char_break = FALSE;
+                  break;
+
+                case BREAK_IF_SPACES:
+                  /* break if prev char was space */
+                  if (prev_was_break_space)                    
+                    attrs[i].is_line_break = TRUE;
+                  break;
+
+                case BREAK_ALLOWED:
+                  attrs[i].is_line_break = TRUE;
+                  break;
+
+                default:
+                  g_assert_not_reached ();
+                  break;
+                }
+            }
+        }
+      
+      if (break_type != G_UNICODE_BREAK_SPACE)
+        {
+          prev_break_type = break_type;
+          prev_was_break_space = FALSE;
+        }
+      else
+        prev_was_break_space = TRUE;
+
+      prev_type = type;
+      prev_wc = wc;
+
+      /* wc might not be a valid unicode base character, but really all we
+       * need to know is the last non-combining character */
+      if (type != G_UNICODE_COMBINING_MARK && 
+          type != G_UNICODE_ENCLOSING_MARK && 
+          type != G_UNICODE_NON_SPACING_MARK)
+        base_character = wc;
+    }
+}
+
+/**
  * pango_default_break:
  * @text: text to break
  * @length: length of text in bytes (may be -1 if @text is nul-terminated)
@@ -1386,6 +1774,25 @@ pango_break (const gchar   *text,
   else
     pango_default_break (text, length, analysis, attrs, attrs_len);
 }
+
+void
+pango_breakfast (const gchar   *text,
+                 gint           length,
+                 PangoAnalysis *analysis,
+                 PangoLogAttr  *attrs,
+                 int            attrs_len)
+{
+  g_return_if_fail (text != NULL);
+  g_return_if_fail (analysis != NULL);
+  g_return_if_fail (attrs != NULL);
+  
+  if (length < 0)
+    length = strlen (text);
+
+  /* do easy break stuff */
+  pango_default_breakfast (text, length, analysis, attrs, attrs_len);
+}
+
 
 /**
  * pango_find_paragraph_boundary:
Index: pango/pango-layout.c
===================================================================
RCS file: /cvs/gnome/pango/pango/pango-layout.c,v
retrieving revision 1.141
diff -p -u -r1.141 pango-layout.c
--- pango/pango-layout.c	15 Aug 2005 15:45:46 -0000	1.141
+++ pango/pango-layout.c	4 Nov 2005 00:25:08 -0000
@@ -134,6 +134,7 @@ static void pango_layout_get_item_proper
 					      ItemProperties *properties);
 
 static void pango_layout_finalize    (GObject          *object);
+static void pango_layout_ensure_log_attrs (PangoLayout *layout);
 
 G_DEFINE_TYPE (PangoLayout, pango_layout, G_TYPE_OBJECT)
 
@@ -152,6 +153,7 @@ pango_layout_init (PangoLayout *layout)
   layout->auto_dir = TRUE;
 
   layout->log_attrs = NULL;
+  layout->log_attrs_initialized = FALSE;
   layout->lines = NULL;
 
   layout->tab_width = -1;
@@ -964,6 +966,7 @@ pango_layout_get_log_attrs (PangoLayout 
   g_return_if_fail (layout != NULL);
 
   pango_layout_check_lines (layout);
+  pango_layout_ensure_log_attrs (layout);
 
   if (attrs)
     {
@@ -2613,7 +2616,6 @@ can_break_at (PangoLayout *layout,
   
   if (wrap == PANGO_WRAP_WORD_CHAR)
     wrap = always_wrap_char ? PANGO_WRAP_CHAR : PANGO_WRAP_WORD;
-  
   if (offset == layout->n_chars)
     return TRUE;
   else if (wrap == PANGO_WRAP_WORD)
@@ -3092,6 +3094,56 @@ get_items_log_attrs (const char   *text,
     }
 }
 
+static void
+get_minimal_items_log_attrs (const char   *text,
+                             GList        *items,
+                             PangoLogAttr *log_attrs,
+                             int           para_delimiter_len)
+{
+  int offset = 0;
+  int index = 0;
+  
+  while (items)
+    {
+      PangoItem tmp_item = *(PangoItem *)items->data;
+
+      /* Accumulate all the consecutive items that match in language
+       * characteristics, ignoring font, style tags, etc.
+       */
+      while (items->next)
+	{
+	  PangoItem *next_item = items->next->data;
+
+	  /* FIXME: Handle language tags */
+	  if (next_item->analysis.lang_engine != tmp_item.analysis.lang_engine)
+	    break;
+	  else
+	    {
+	      tmp_item.length += next_item->length;
+	      tmp_item.num_chars += next_item->num_chars;
+	    }
+
+	  items = items->next;
+	}
+
+      /* Break the paragraph delimiters with the last item */
+      if (items->next == NULL)
+	{
+	  tmp_item.num_chars += g_utf8_strlen (text + index + tmp_item.length, para_delimiter_len);
+	  tmp_item.length += para_delimiter_len;
+	}
+
+      pango_breakfast (text + index, tmp_item.length, &tmp_item.analysis,
+                       log_attrs + offset, tmp_item.num_chars + 1);
+
+      offset += tmp_item.num_chars;
+      index += tmp_item.length;
+      
+      items = items->next;
+    }
+}
+
+
 static PangoAttrList *
 pango_layout_get_effective_attributes (PangoLayout *layout)
 {
@@ -3267,9 +3319,9 @@ pango_layout_check_lines (PangoLayout *l
 						 attrs,
 						 iter);
 
-      get_items_log_attrs (start, state.items,
-                           layout->log_attrs + start_offset,
-                           delim_len);
+      get_minimal_items_log_attrs (start, state.items,
+                                   layout->log_attrs + start_offset,
+                                   delim_len);
 
       if (state.items)
 	{
@@ -5236,3 +5288,107 @@ pango_layout_iter_get_layout_extents  (P
   if (logical_rect)
     *logical_rect = iter->logical_rect;
 }
+
+static void
+pango_layout_ensure_log_attrs (PangoLayout *layout)
+{
+  const char *start;
+  gboolean done = FALSE;
+  int start_offset;
+  PangoAttrList *attrs;
+  PangoAttrList *no_shape_attrs;
+  PangoAttrIterator *iter;
+  PangoDirection prev_base_dir = PANGO_DIRECTION_NEUTRAL, base_dir = PANGO_DIRECTION_NEUTRAL;
+
+  if (layout->log_attrs_initialized) return;
+  layout->log_attrs_initialized = TRUE;
+
+  attrs = pango_layout_get_effective_attributes (layout);
+  no_shape_attrs = filter_no_shape_attributes (attrs);
+  iter = pango_attr_list_get_iterator (attrs);
+  
+  start_offset = 0;
+  start = layout->text;
+  
+  /* Find the first strong direction of the text */
+  if (layout->auto_dir)
+    {
+      prev_base_dir = pango_find_base_dir (layout->text, layout->length);
+      if (prev_base_dir == PANGO_DIRECTION_NEUTRAL)
+	prev_base_dir = pango_context_get_base_dir (layout->context);
+    }
+  else
+    base_dir = pango_context_get_base_dir (layout->context);
+
+  do
+    {
+      int delim_len;
+      const char *end;
+      int delimiter_index, next_para_index;
+      ParaBreakState state;
+
+      if (layout->single_paragraph)
+        {
+          delimiter_index = layout->length;
+          next_para_index = layout->length;
+        }
+      else
+        {
+          pango_find_paragraph_boundary (start,
+                                         (layout->text + layout->length) - start,
+                                         &delimiter_index,
+                                         &next_para_index);
+        }
+
+      g_assert (next_para_index >= delimiter_index);
+
+      if (layout->auto_dir)
+	{
+	  base_dir = pango_find_base_dir (start, delimiter_index);
+	  
+	  /* Propagate the base direction for neutral paragraphs */
+	  if (base_dir == PANGO_DIRECTION_NEUTRAL)
+	    base_dir = prev_base_dir;
+	  else
+	    prev_base_dir = base_dir;
+	}
+
+      end = start + delimiter_index;
+      
+      delim_len = next_para_index - delimiter_index;
+      
+      if (end == (layout->text + layout->length))
+	done = TRUE;
+
+      g_assert (end <= (layout->text + layout->length));
+      g_assert (start <= (layout->text + layout->length));
+      g_assert (delim_len < 4);	/* PS is 3 bytes */
+      g_assert (delim_len >= 0);
+
+      state.attrs = attrs;
+      state.items = pango_itemize_with_base_dir (layout->context,
+						 base_dir,
+						 layout->text,
+						 start - layout->text,
+						 end - start,
+						 attrs,
+						 iter);
+
+      get_items_log_attrs (start, state.items,
+                           layout->log_attrs + start_offset,
+                           delim_len);
+
+      while (state.items)
+          state.items = g_list_delete_link (state.items, state.items);
+
+      if (!done)
+        start_offset += g_utf8_strlen (start, (end - start) + delim_len);
+
+      start = end + delim_len;
+    }
+  while (!done);
+
+  pango_attr_iterator_destroy (iter);
+  pango_attr_list_unref (attrs);
+}
+
